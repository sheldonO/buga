import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <>
      <section>
        <footer>
          <div className="container footer-section">
            <div className="footer-links foot-des">
              <img src="./images/test.png" />
              <p>
                Buga is an application to handle all your deliveries. We have
                partnered with qualified drivers to provide you with efficient
                services. With our tracking feature, you are well inform with
                your parcel.
              </p>
            </div>

            <div className="footer-links">
              <h2>Our Information</h2>
              <p>Return Policy</p>
              <p>Privacy Policy</p>
              <p>Terms & Conditions</p>
              <p>Site Map</p>
            </div>

            <div className="footer-links">
              <h2>Policy</h2>
              <p>Application Security</p>
              <p>Software Principles</p>
              <p>Unwanted software policy</p>
              <p>Responsible supply chain</p>
            </div>
          </div>
          <p
            className="text-center"
            style={{ color: "#fff", paddingBottom: "1rem" }}
          >
            All right reserved - Design & Developed buga.
          </p>
        </footer>
      </section>
    </>
  );
}

export default Footer;
