import Footer from "../components/Footer";
import "./SignUp.css";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  Box,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import Axios from "axios";

import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { LoadingButton } from "@mui/lab";
import { AccountPlusOutline } from "mdi-material-ui";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function SignUp() {
const {
  register,
  handleSubmit,
  reset,
  formState: { errors },
  getValues,
} = useForm();

const [error, setError] = useState(false);
const [open, setOpen] = useState(false);
const [message, setMessage] = useState("");
const [isLoading, setIsLoading] = useState(false);
const getNipost = () => (getValues("nipost") === "yes" ? true : false);

//closes the snackbar
const handleClose = (event, reason) => {
  if (reason === "clickaway") {
    return;
  }

  setOpen(false);
};

//checks if passwords match
const isPasswordMatch = () =>
  getValues("password") != getValues("conPassword") ? false : true;

//submits the driver form to create an account
const onSubmit = () => {
  if (isPasswordMatch()) {
    setIsLoading(true);
    setError(false);

    const userData = {
      address: getValues("address"),
      driversLicence: getValues("licence"),
      email: getValues("email"),
      firstname: getValues("firstname"),
      gender: getValues("gender"),
      lastname: getValues("lastname"),
      maritalStatus: getValues("maritalStatus"),
      nipostOption: getNipost(),
      password: getValues("password"),
      phone: getValues("phoneNumber"),
      state: getValues("state"),
      username: getValues("username"),
      vehicleType: getValues("vehicleType"),
    };

    try {
      setTimeout(() => {
        Axios.post(
          "https://apps-1.lampnets.com/buga/profiles/addnewdriveraccount",
          userData
        )
          .then((res) => {
            if ((res.status = 200)) {
              setIsLoading(false);
              setOpen(true);
              setMessage(res.data);

              if (res.data != "driver already exist") {
                reset();
              }
            }
          })
          .catch((err) => {
            setIsLoading(false);
            setError(true);
            if ((err.response.status = 403)) {
              setMessage(err.response.data.error);
            } else if ((err.response.status = 500)) {
              return;
            }
          });
      }, 3000);
    } catch (err) {
      console.log(err.message);
      setIsLoading(false);
      setOpen(true);
      setMessage(err.message);
    }
  }
};
  return (
    <>
      <section>
        <div className="container sign-up-section">
          <div className="row">
            <div className="col-md-5">
              <div
                style={{
                  marginTop: "3rem",
                  color: "#0c6a52",
                  fontWeight: "bold",
                }}
              >
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: "3rem",
                  }}
                >
                  SignUp To Join Our List Of Riders
                </p>

                <img src="./images/corier.jpg" className="img-fluid" />
              </div>
            </div>

            <div className="col-md-7">
              <div
                style={{
                  marginTop: "4rem",
                }}
              >
                <Box>
                  <Snackbar
                    open={open}
                    autoHideDuration={6000}
                    onClose={handleClose}
                  >
                    <Alert
                      onClose={handleClose}
                      severity={
                        error
                          ? "error"
                          : message == "driver already exist"
                          ? "info"
                          : "success"
                      }
                      sx={{ width: "100%", color: "white" }}
                    >
                      {error ? message : message}
                    </Alert>
                  </Snackbar>

                  <Box
                    sx={{
                      position: "relative",
                    }}
                  >
                    <form onSubmit={handleSubmit(onSubmit)}>
                      <Grid container spacing={2} style={{ padding: "2rem" }}>
                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Firstname"
                              variant="outlined"
                              {...register("firstname", {
                                required: true,
                                maxLength: 20,
                              })}
                              error={
                                errors?.firstname?.type === "required" ||
                                errors?.firstname?.type === "maxLength"
                              }
                              helperText={
                                errors?.firstname?.type === "required"
                                  ? "This field is required"
                                  : errors?.firstname?.type === "maxLength"
                                  ? "Max. characters is 20"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Lastname"
                              variant="outlined"
                              {...register("lastname", {
                                required: true,
                                maxLength: 20,
                              })}
                              error={
                                errors?.lastname?.type === "required" ||
                                errors?.lastname?.type === "maxLength"
                              }
                              helperText={
                                errors?.lastname?.type === "required"
                                  ? "This field is required"
                                  : errors?.lastname?.type === "maxLength"
                                  ? "Max. characters is 20"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Username"
                              variant="outlined"
                              {...register("username", {
                                required: true,
                                maxLength: 20,
                              })}
                              error={
                                errors?.username?.type === "required" ||
                                errors?.username?.type === "maxLength"
                              }
                              helperText={
                                errors?.username?.type === "required"
                                  ? "This field is required"
                                  : errors?.username?.type === "maxLength"
                                  ? "Max. characters is 20"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Email"
                              variant="outlined"
                              {...register("email", {
                                required: true,
                                pattern:
                                  /^[A-Za-z0-9_!#$%&'*+\/=?`{|}~^.-]+@[A-Za-z0-9.-]+$/gm,
                              })}
                              error={
                                errors?.email?.type === "required" ||
                                errors?.email?.type === "pattern"
                              }
                              helperText={
                                errors?.email?.type === "required"
                                  ? "This field is required"
                                  : errors?.email?.type === "pattern"
                                  ? "Enter valid email"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Phone Number"
                              variant="outlined"
                              {...register("phoneNumber", {
                                required: true,
                              })}
                              error={errors?.phoneNumber?.type === "required"}
                              helperText={
                                errors?.phoneNumber?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Address"
                              variant="outlined"
                              {...register("address", {
                                required: true,
                              })}
                              error={errors?.address?.type === "required"}
                              helperText={
                                errors?.address?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">
                              Select gender
                            </InputLabel>

                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              {...register("gender", {
                                required: true,
                              })}
                              error={errors?.gender?.type === "required"}
                              helperText={
                                errors?.gender?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            >
                              <MenuItem disabled value="">
                                <em>Select gender</em>
                              </MenuItem>
                              <MenuItem value="male">Male</MenuItem>
                              <MenuItem value="female">Female</MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">
                              Marital Status
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              {...register("maritalStatus", {
                                required: true,
                              })}
                              error={errors?.maritalStatus?.type === "required"}
                              helperText={
                                errors?.maritalStatus?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            >
                              <MenuItem value={"married"}>Married</MenuItem>
                              <MenuItem value={"single"}>Single</MenuItem>
                              <MenuItem value={"divorced"}>Divorced</MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="State"
                              variant="outlined"
                              {...register("state", {
                                required: true,
                              })}
                              error={errors?.state?.type === "required"}
                              helperText={
                                errors?.state?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Vehicle Type"
                              variant="outlined"
                              {...register("vehicleType", {
                                required: true,
                              })}
                              error={errors?.vehicleType?.type === "required"}
                              helperText={
                                errors?.vehicleType?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Licence"
                              variant="outlined"
                              {...register("licence", {
                                required: true,
                              })}
                              error={errors?.licence?.type === "required"}
                              helperText={
                                errors?.licence?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">
                              Nipost
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              {...register("nipost", {
                                required: true,
                              })}
                              error={errors?.nipost?.type === "required"}
                              helperText={
                                errors?.nipost?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            >
                              <MenuItem value={"yes"}>yes</MenuItem>
                              <MenuItem value={"no"}>no</MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Password"
                              variant="outlined"
                              {...register("password", {
                                required: true,
                              })}
                              error={errors?.password?.type === "required"}
                              helperText={
                                errors?.password?.type === "required"
                                  ? "This field is required"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <Grid item xs={12} md={6}>
                          <FormControl fullWidth>
                            <TextField
                              id="outlined-basic"
                              label="Confirm Password"
                              variant="outlined"
                              {...register("conPassword", {
                                required: true,
                              })}
                              error={
                                errors?.conPassword?.type === "required" ||
                                isPasswordMatch() == false
                              }
                              helperText={
                                errors?.conPassword?.type === "required"
                                  ? "This field is required"
                                  : !isPasswordMatch()
                                  ? "Password do not match"
                                  : ""
                              }
                            />
                          </FormControl>
                        </Grid>

                        <LoadingButton
                          loading={isLoading}
                          loadingPosition="start"
                          startIcon={<AccountPlusOutline />}
                          variant="contained"
                          sx={{ margin: "2rem" }}
                          color="success"
                          type="submit"
                        >
                          Register
                        </LoadingButton>
                      </Grid>
                    </form>
                  </Box>
                </Box>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
}

export default SignUp;
